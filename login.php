<!DOCTYPE html>
<html>
<title>Hotel</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="style/w33.css">
<link href='style/ccc.css' rel='stylesheet' type='text/css'>

<head>
<style>

.login {
  margin: 20px auto;
  width: 300px;
  padding: 30px 25px;
  background: white;
  border: 1px solid #c4c4c4;
}

h1.login-title {
  margin: -28px -25px 25px;
  padding: 15px 25px;
  line-height: 30px;
  font-size: 25px;
  font-weight: 300;
  color: #ADADAD;
  text-align:center;
  background: #f7f7f7;
 
}

.login-input {
  width: 260px;
  height: 50px;
  margin-bottom: 25px;
  padding-left:10px;
  font-size: 15px;
  background: #fff;
  border: 1px solid #ccc;
  border-radius: 4px;
}
.login-input:focus {
    border-color:#6e8095;
    outline: none;
  }
.login-button {
  width: 100%;
  height: 50px;
  padding: 0;
  font-size: 20px;
  color: #fff;
  text-align: center;
  background: #f0776c;
  border: 0;
  border-radius: 5px;
  cursor: pointer; 
  outline:0;
}

.login-lost
{
  text-align:center;
  margin-bottom:0px;
}

.login-lost a
{
  color:#666;
  text-decoration:none;
  font-size:13px;
}
</style>
</head>


<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card">
    <a href="index.php" class="bride">Home</a>
    <!-- Float links to the right. Hide them on small screens -->
	<div class="w3-dropdown-hover">
	<button class="w3-button w3-black">Hotel</button>

	<div class="w3-dropdown-content w3-bar-block w3-border">
      <a href="Rooms.php" class="w3-bar-item w3-button">Rooms</a>
      <a href="ReservationForm.php" class="w3-bar-item w3-button">Reservation</a>
      <a href="#" class="w3-bar-item w3-button">Hotel location</a>
	  </div>


    </div>

	<div class="w3-right w3-hide-small">

      <a href="login.php" class="w3-bar-item w3-button">Login</a>
	  <a href="Registration.php" class="w3-bar-item w3-button">SignUp</a>

</div>
  </div>
</div>


</br>
</br>
</br>
</br>
</br>
</br>
<!-- Header -->
<header class="w3-display-container w3-content w3-wide" style="max-width:1500px;" id="home">
<form class="login">
    <h1 class="login-title">Login</h1>
    <input type="text" class="login-input" placeholder="Email Adress" autofocus>
    <input type="password" class="login-input" placeholder="Password">
    <input type="submit" value="Go" class="login-button">
  <p class="login-lost"><a href="">Forgot Password?</a></p>
  </form>

</header>

</br>
</br>
</br>

<!-- Footer -->
<footer class="w3-center w3-black2 w3-padding-16">
  <p>Powered by <a title="W3.CSS" target="_blank" class="w3-hover-text-green">Shaira Jonas Gian</a></p>
</footer>

</body>
</html>
