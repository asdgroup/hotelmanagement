<!DOCTYPE html>
<html>
<title>Hotel</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="style/w33.css">
<link href='style/ccc.css' rel='stylesheet' type='text/css'>


<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card">
    <a href="index.php" class="bride">Home</a>
    <!-- Float links to the right. Hide them on small screens -->
	<div class="w3-dropdown-hover">
	<button class="w3-button w3-black">Hotel</button>

	<div class="w3-dropdown-content w3-bar-block w3-border">
      <a href="Rooms.php" class="w3-bar-item w3-button">Rooms</a>
      <a href="ReservationForm.php" class="w3-bar-item w3-button">Reservation</a>
      <a href="#" class="w3-bar-item w3-button">Hotel location</a>
	  </div>


    </div>

	<div class="w3-right w3-hide-small">

      <a href="login.php" class="w3-bar-item w3-button">Login</a>
	  <a href="Registration.php" class="w3-bar-item w3-button">SignUp</a>

</div>
  </div>
</div>
<!-- Header -->
<header class="w3-display-container w3-content w3-wide" style="max-width:1500px;" id="home">
  <img class="w3-image" src="h3.jpg" alt="wed" width="1500" height="800">
</header>



<!-- Footer -->
<footer class="w3-center w3-black2 w3-padding-16">
  <p>Powered by <a title="W3.CSS" target="_blank" class="w3-hover-text-green">Shaira Jonas Gian</a></p>
</footer>

</body>
</html>
